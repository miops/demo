# 1. Containerisation

Use make command to build the container. Its been tested on macos using docker desktop
### to test the build
make docker_image;

### to test the runtime
make docker_testrun;

# 2. Environment
Two seperate containers will be created with one running app at 8880 and other running same app on 9990.
By running below make command , docker compose code can be tested in which one app is connecting the other app and vice versa.

# 3. Deliverable
### to test the environment
make testenv;

# 4. Pipeline
I have created Jenkinsfile for build the artifactes and creating docker image from it. 
